import React from "react";

import AppContainer from "../../container/App";
import { routes } from "../../config/routes/routes";
import Container from "react-bootstrap/cjs/Container";

import TimeTableContainer from "../../container/TimeTable";

const HomePage = () => {
  return (
    <AppContainer pageTitle={routes.home.title}>
      <Container className="mt-2">
        <TimeTableContainer />
      </Container>
    </AppContainer>
  );
};

export default HomePage;
