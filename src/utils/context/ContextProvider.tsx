import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { QueryParamProvider } from "use-query-params";
import { HelmetProvider } from "react-helmet-async";

// Types
import { ReactNode } from "react";
type Props = {
  children: ReactNode;
};
const ContextProvider = ({ children }: Props) => {
  return (
    <Router>
      <QueryParamProvider ReactRouterRoute={Route}>
        <HelmetProvider>{children}</HelmetProvider>
      </QueryParamProvider>
    </Router>
  );
};

export default ContextProvider;
