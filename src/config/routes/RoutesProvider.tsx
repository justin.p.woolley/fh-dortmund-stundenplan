import React from "react";
import { Route, Switch } from "react-router";

import HomePage from "../../pages/Home";

import { routes } from "./routes";

const RoutesProvider = () => {
  return (
    <Switch>
      <Route exact={true} path={routes.home.path} component={HomePage} />
    </Switch>
  );
};

export default RoutesProvider;
