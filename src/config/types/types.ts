export type TimeTableType = {
  courseId: string;
  courseType: string;
  dateBegin: string;
  dateEnd: string;
  lecturerName: string;
  name: string;
  roomId: string;
  studentSet: string;
  timeBegin: string;
  timeEnd: string;
  weekday: string;
};
