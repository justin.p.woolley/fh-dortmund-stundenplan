import React from "react";
import { Helmet } from "react-helmet-async";

// Types
import { ReactNode } from "react";

type Props = {
  children: ReactNode;
  pageTitle: string;
};
const AppContainer = ({ children, pageTitle }: Props) => {
  return (
    <>
      <Helmet>
        <title>{pageTitle}</title>
      </Helmet>
      {children}
    </>
  );
};

export default AppContainer;
