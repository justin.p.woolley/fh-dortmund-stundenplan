import axios from "axios";

// Types
import { TimeTableType } from "../../config/types/types";

export const fetcher = (_key: string): Promise<TimeTableType[]> => {
  return axios
    .get(
      "https://ws.inf.fh-dortmund.de/fbws/current/rest/CourseOfStudy/STDBSW/3/Events?Accept=application/json&studentSet=*"
    )
    .then((r) => {
      return r.data;
    });
};
