import React from "react";

// Types
import { TimeTableType } from "../../../config/types/types";
import TimeTableCard from "../../../components/TimeTableCard";
import Row from "react-bootstrap/cjs/Row";

type Props = {
  data: TimeTableType[];
  weekday: string | null;
};

const formatTime = (time: String) => {
  let timeArray = time.split("");
  let returnValue = "";
  switch (timeArray.length) {
    case 3:
      returnValue = timeArray[0] + ":" + timeArray[1] + timeArray[2];
      break;
    case 4:
      returnValue =
        timeArray[0] + timeArray[1] + ":" + timeArray[2] + timeArray[3];
  }
  return returnValue;
};

const TimeTableCardContainer = ({ data, weekday }: Props) => {
  return (
    <React.Fragment key={weekday}>
      <Row>
        {data.map((item) => {
          return (
            <TimeTableCard
              studentSet={item.studentSet}
              timeEnd={formatTime(item.timeEnd)}
              timeStart={formatTime(item.timeBegin)}
              name={item.name}
              courseType={item.courseType}
              lecturer={item.lecturerName}
              room={item.roomId}
            />
          );
        })}
      </Row>
    </React.Fragment>
  );
};

export default TimeTableCardContainer;
