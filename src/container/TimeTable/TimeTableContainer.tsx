import React from "react";
import useSWR from "swr";

import { fetcher } from "./fetcher";

import Tabs from "react-bootstrap/cjs/Tabs";
import Tab from "react-bootstrap/cjs/Tab";

import TimeTableCardContainer from "./TimeTableCard";

const TimeTableContainer = () => {
  const { data } = useSWR("Stundenplan", fetcher);
  const [weekday, setWeekday] = React.useState<string | null>("Mon");

  if (!data) {
    return <p>Es ist ein Fehler aufgetreten</p>;
  }
  return (
    <>
      <Tabs
        id="controlled-tab-example"
        activeKey={weekday}
        onSelect={(k) => setWeekday(k)}
      >
        <Tab eventKey="Mon" title="Montag" />

        <Tab eventKey="Tue" title="Dienstag" />

        <Tab eventKey="Wed" title="Mittwoch" />
        <Tab eventKey="Thu" title="Donnerstag" />
        <Tab eventKey="Fri" title="Freitag" />
      </Tabs>
      <TimeTableCardContainer
        data={data.filter((item) => {
          return item.weekday === weekday;
        })}
        weekday={weekday}
      />
    </>
  );
};

export default TimeTableContainer;
