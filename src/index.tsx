import React from "react";
import ReactDOM from "react-dom";
import * as serviceWorker from "./serviceWorker";
import { Helmet } from "react-helmet-async";
import "bootstrap/dist/css/bootstrap.min.css";

import ContextProvider from "./utils/context";
import RoutesProvider from "./config/routes/RoutesProvider";

const App = () => {
  return (
    <ContextProvider>
      <Helmet titleTemplate="%s" />

      <RoutesProvider />
    </ContextProvider>
  );
};

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
