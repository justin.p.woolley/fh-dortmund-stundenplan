import React from "react";
import Card from "react-bootstrap/cjs/Card";

type Props = {
  studentSet: string;
  courseType: string;
  lecturer: string;
  name: string;
  room: string;
  timeEnd: string;
  timeStart: string;
};

const TimeTableCard = ({
  studentSet,
  courseType,
  lecturer,
  name,
  room,
  timeEnd,
  timeStart,
}: Props) => {
  return (
    <Card style={{ width: "18rem" }} className="ml-2 mt-2">
      <Card.Body>
        <Card.Title>
          {name} {courseType}
        </Card.Title>
        <Card.Subtitle className="mb-2 text-muted">
          {lecturer} {studentSet}
        </Card.Subtitle>
        <Card.Text>
          {timeStart} Uhr - {timeEnd} Uhr
        </Card.Text>
        <Card.Text>{room}</Card.Text>
      </Card.Body>
    </Card>
  );
};

export default TimeTableCard;
